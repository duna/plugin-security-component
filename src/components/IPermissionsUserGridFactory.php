<?php

namespace Duna\Plugin\SecurityComponent\Components;

use Nette\Application\UI\Control;

interface IPermissionsUserGridFactory
{
    /**
     * @param \Nette\Application\UI\Control $parent
     * @param $name
     * @param array $filters
     * @return PermissionsUserGrid\Component
     */
function create(Control $parent, $name, array $filters);
}