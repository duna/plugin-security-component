<?php

namespace Duna\Plugin\SecurityComponent\Facade;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Plugin\SecurityComponent\Entity\Component;
use Nette\InvalidStateException;

class ComponentFacade
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getAll()
    {
        return $this->em->getRepository(Component::class)->findAll();
    }

    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository(Component::class)->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function getOneByResource($resource, $throwException = false)
    {
        $entity = $this->em->getRepository(Component::class)->findOneBy([
            'resource' => $resource,
        ]);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @param $name
     * @param $resource
     * @param bool $throwException
     * @return \Duna\Plugin\SecurityComponent\Entity\Component
     */
    public function insert($name, $resource, $throwException = false)
    {
        $entity = new Component();
        $entity->name = $name;
        $entity->resource = $resource;
        $this->em->persist($entity);

        try {
            $this->em->flush($entity);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    public function getByFilter(array $filters) {
        $qb = $this->em->createQueryBuilder();
        $qb->select("comp")
            ->from(Component::class, 'comp');

        foreach ($filters['where'] as $filter) {
            $qb->add('where', $filter);
        }
        foreach ($filters['params'] as $key => $value) {
            $qb->setParameter($key, $value);
        }

        return $qb->getQuery()->getResult();
    }
}