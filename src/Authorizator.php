<?php

namespace Duna\Plugin\SecurityComponent;

use Duna\Plugin\SecurityComponent\Entity\Component;
use Duna\Security\Entity\Role;
use Duna\Security\Entity\User;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\InvalidStateException;
use Nette\Security\Permission;

class Authorizator implements \Nette\Security\IAuthorizator
{
    const CREATE = 'create';
    const READ = 'read';
    const UPDATE = 'update';
    const DELETE = 'delete';
    const CACHE_NAMESPACE = 'duna.plugin.security-component';

    /** @var  \Kdyby\Doctrine\EntityManager */
    private $em;
    /** @var  \Nette\Caching\Cache */
    private $cache;
    /** @var  \Nette\Caching\Cache */
    private $cacheSecurity;

    /** @var  \Nette\Security\Permission */
    private $permission;

    /**
     * Check if $name is valid operation
     *
     * @param $name
     * @return string|null
     */
    public static function getOperation($name)
    {
        switch ($name) {
            case self::CREATE:
            case self::READ:
            case self::UPDATE:
            case self::DELETE:
                return $name;
            default:
                return null;
        }
    }


    public function __construct(EntityManager $em, IStorage $storage)
    {
        $this->em = $em;
        $this->cache = new Cache($storage, self::CACHE_NAMESPACE);
        $this->cacheSecurity = new Cache($storage, \Duna\Security\Authorizator::CACHE_NAMESPACE);
        $this->permission = new Permission();

        if (PHP_SAPI === 'cli')
            return;

        $roles = $this->cacheSecurity->load('roles', function (&$dep) {
            $dep = [Cache::TAGS => [\Duna\Security\Authorizator::CACHE_NAMESPACE . '/roles']];
            return $this->em->getRepository(Role::class)->findAll();
        });
        foreach ($roles as $role) {
            $this->permission->addRole($role->name, $role->getParent() ? $role->getParent()->name : null);
        }

        $users = $this->cacheSecurity->load('users', function (&$dep) {
            $dep = [Cache::TAGS => [\Duna\Security\Authorizator::CACHE_NAMESPACE . '/users']];
            return $this->em->getRepository(User::class)->findAll();
        });
        foreach ($users as $user) {
            $this->permission->addRole($user->email);
        }

        $components = $this->cache->load('components', function (&$dep) {
            $dep = [Cache::TAGS => [self::CACHE_NAMESPACE . '/components']];
            return $this->em->getRepository(Component::class)->findAll();
        });
        foreach ($components as $resource) {
            $this->permission->addResource($resource->resource);
        }

        $permissions = $this->cache->load('permissions', function (&$dep) {
            $dep = [Cache::TAGS => [self::CACHE_NAMESPACE . '/permissions']];
            $qb = $this->em->getRepository(\Duna\Plugin\SecurityComponent\Entity\Permission::class)->createQueryBuilder('perm');
            return $qb->join('perm.component', 'comp')->addSelect('comp')
                ->leftJoin('perm.role', 'role')->addSelect('role')
                ->leftJoin('perm.user', 'user')->addSelect('user')
                ->getQuery()->getResult();
        });

        foreach ($permissions as $permission) {
            if ($permission->user !== null) {
                if ($permission->create)
                    $this->permission->allow($permission->user->email, $permission->component->resource, self::CREATE);
                else
                    $this->permission->deny($permission->user->email, $permission->component->resource, self::CREATE);

                if ($permission->read)
                    $this->permission->allow($permission->user->email, $permission->component->resource, self::READ);
                else
                    $this->permission->deny($permission->user->email, $permission->component->resource, self::READ);

                if ($permission->update)
                    $this->permission->allow($permission->user->email, $permission->component->resource, self::UPDATE);
                else
                    $this->permission->deny($permission->user->email, $permission->component->resource, self::UPDATE);

                if ($permission->delete)
                    $this->permission->allow($permission->user->email, $permission->component->resource, self::DELETE);
                else
                    $this->permission->deny($permission->user->email, $permission->component->resource, self::DELETE);
            }
        }
        $this->permission->deny(\Nette\Security\Permission::ALL, \Nette\Security\Permission::ALL, \Nette\Security\Permission::ALL);


    }

    function isAllowed($role, $resource, $privilege)
    {
        try {
            if (!in_array($resource, $this->permission->getResources()))
                return true;
            return $this->permission->isAllowed($role, $resource, $privilege);
        } catch (InvalidStateException $ex) {
            return false;
        }
    }

}