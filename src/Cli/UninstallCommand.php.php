<?php

namespace Duna\Plugin\SecurityComponent\Cli;

use Duna\Console\DelegateCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UninstallCommand extends DelegateCommand
{
    /** @var \Kdyby\Doctrine\Tools\CacheCleaner @inject */
    public $cacheCleaner;

    /**
     * {@inheritdoc}
     */
    protected function createCommand()
    {
        return new Commands\UninstallCommand();
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        if ($this->cacheCleaner)
            $this->cacheCleaner->invalidate();
    }

}