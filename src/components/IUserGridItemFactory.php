<?php

namespace Duna\Plugin\SecurityComponent\Components;

use Duna\Plugin\SecurityComponent\Entity\Permission;

interface IUserGridItemFactory
{
    /**
     * @param $user
     * @param Permission[] $permissions
     * @param array $filters
     * @return UserGridItem\Component
     */
    function create($user, array $permissions, array $filters);
}