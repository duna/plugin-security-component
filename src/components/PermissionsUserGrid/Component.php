<?php

namespace Duna\Plugin\SecurityComponent\Components\PermissionsUserGrid;

use Duna\Plugin\SecurityComponent\Authorizator;
use Duna\Plugin\SecurityComponent\Components\IUserGridItemFactory;
use Duna\Plugin\SecurityComponent\Entity\PermissionRole;
use Duna\Plugin\SecurityComponent\Facade\ComponentFacade;
use Duna\Plugin\SecurityComponent\Facade\PermissionFacade;
use Duna\Security\Facade\UserFacade;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Utils\ArrayHash;

class Component extends Control
{
    /** @var \Kdyby\Doctrine\EntityManager */
    private $em;
    /** @var Cache */
    private $cache;
    private $users = [];
    /** @var  \Duna\Plugin\SecurityComponent\Facade\PermissionFacade */
    private $facade;
    private $facadeComponent;
    private $facadeUser;
    /** Array of where criterias */
    private $filters = [];
    private $permissions = [];
    /** @var \Duna\Plugin\SecurityComponent\Components\IUserGridItemFactory  */
    private $factory;

    public function __construct(Control $parent, $name, array $filters, EntityManager $em, IStorage $storage, IUserGridItemFactory $factory)
    {
        parent::__construct($parent, $name);
        $this->em = $em;
        $this->filters = $filters;
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
        $this->facade = new PermissionFacade($this->em, $storage);
        $this->facadeComponent = new ComponentFacade($this->em);
        $this->facadeUser = new UserFacade($this->em);

        $this->factory = $factory;
        $this->users = $this->facade->getUsers($this->filters);

        $qb = $this->em->createQueryBuilder()
            ->select('perm')
            ->from(\Duna\Plugin\SecurityComponent\Entity\Permission::class, 'perm');
        $qb->join('perm.component', 'comp')->addSelect('comp')
            ->join('perm.user', 'user')->addSelect('user')
            ->orderBy('comp.name');
        foreach ($this->filters['where'] as $filter) {
            $qb->add('where', $filter);
        }
        foreach ($this->filters['params'] as $key => $value) {
            $qb->setParameter($key, $value);
        }
        $permissions = $qb->getQuery()
            ->getResult();

        foreach ($permissions as $permission) {
            $this->permissions[$permission->user->id][] = $permission;
        }
    }

    public function render()
    {
        $template = $this->template;
        $template->users = $this->users;
        $template->render(__DIR__ . '/default.latte');
    }

    protected function createComponentAclForm()
    {
        $that = $this;
        return new Multiplier(function ($id) use ($that) {
            return $that->factory->create((array) $that->users[$id], (isset($that->permissions[$that->users[$id][0]->id])?$that->permissions[$that->users[$id][0]->id]:[]), $that->filters);
        });
    }

}