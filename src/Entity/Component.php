<?php

namespace Duna\Plugin\SecurityComponent\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_sec_com_component", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name", "resource"})})
 * @ORM\Entity
 */
class Component
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $resource;

    /**
     * @var \Duna\Plugin\SecurityComponent\Entity\Permission[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Permission", mappedBy="component", cascade={"persist"})
     */
    protected $privileges;
    /**
     * @var integer
     *
     * @ORM\Column(name="sec_com_component_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function __construct()
    {
        $this->privileges = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}