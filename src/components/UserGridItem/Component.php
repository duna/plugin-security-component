<?php

namespace Duna\Plugin\SecurityComponent\Components\UserGridItem;

use Doctrine\ORM\EntityManagerInterface;
use Duna\Plugin\SecurityComponent\Components\IPermissionUserFormFactory;
use Duna\Plugin\SecurityComponent\Entity\Permission;
use Duna\Plugin\SecurityComponent\Entity\PermissionRole;
use Duna\Plugin\SecurityComponent\Facade\ComponentFacade;
use Nette\Application\UI\Control;
use Nette\Caching\IStorage;

class Component extends Control
{
    private $userData;
    private $permissions = [];
    private $em;
    private $filters = [];
    private $factory;

    public function __construct($user, array $permissions, array $filters, IStorage $storage, EntityManagerInterface $em, IPermissionUserFormFactory $factory)
    {
        $this->em = $em;
        $this->userData = $user;

        if (count($permissions) === 0) {
            $facade = new ComponentFacade($this->em);
            foreach ($facade->getByFilter($filters) as $component) {
                $entity = new Permission();
                $entity->user = $user[0];
                $entity->component = $component;
                $this->permissions[] = $entity;
            }
        } else $this->permissions = $permissions;
        $this->filters = $filters;
        $this->factory = $factory;
    }

    public function formError(\Duna\Forms\Form $form)
    {
        // TODO: Implement formError() method.
    }

    public function formSuccess(\Duna\Forms\Form $form, $values)
    {
        // TODO: Implement formSuccess() method.
    }

    public function render()
    {
        $template = $this->template;
        $template->userData = $this->userData;
        $template->render(__DIR__ . '/default.latte');
    }

    protected function createComponentPermissions($name)
    {
        $control = $this->factory->create($this, $name, $this->permissions);
        $that = $this;
        $control->onCreate[] = function () use ($that) {
            $this->userData['components'] = 1;
            $that->redrawControl();
        };
        return $control;
    }


}