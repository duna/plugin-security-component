<?php

namespace Duna\Plugin\SecurityComponent\Cli\Commands;

use Doctrine\ORM\Tools\ToolsException;
use Duna\Console\Command;
use Duna\Plugin\Manager\Facade;
use Duna\Plugin\SecurityComponent\DI\Extension;

class UninstallCommand extends Command
{
    public function getDependentCommands()
    {
        return [
            ['command' => 'plugin:manager:install'],
        ];
    }

    public function getEntityMappings($onlyKey = false, $data = [])
    {
        return parent::getEntityMappings($onlyKey, Extension::entityMappings());
    }

    public function getExtension()
    {
        return Extension::class;
    }

    public function getTitle()
    {
        $entity = Extension::getPluginInfo()['plugin'];
        return $entity->description;
    }

    public function runCommand()
    {
        $facade = new Facade($this->em);
        $result = $this->getMetadataTables(true);
        try {
            $this->addMessageInfo('Dropping database schema...');
            $this->schemaTool->dropSchema($result);
            $entity = Extension::getPluginInfo()['plugin'];
            $facade->deleteByHash(md5($entity->name));
            return 0;
        } catch (ToolsException $e) {
            return 1;
        }
    }

    protected function configure()
    {
        $this->setName('plugin:security-component:uninstall')
            ->setDescription('Create database schema and registred plugin.');
    }

}