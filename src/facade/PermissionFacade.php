<?php

namespace Duna\Plugin\SecurityComponent\Facade;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Plugin\SecurityComponent\Authorizator;
use Duna\Plugin\SecurityComponent\Entity\Component;
use Duna\Plugin\SecurityComponent\Entity\Permission;
use Duna\Security\Entity\Role;
use Duna\Security\Entity\User;
use Duna\Security\Facade\RoleFacade;
use Duna\Security\Facade\UserFacade;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\InvalidStateException;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class PermissionFacade
{
    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $em;
    /** @var \Nette\Caching\Cache */
    private $cache;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        $this->em = $em;
        if ($storage)
            $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
    }

    public function getUsers(array $filters)
    {
        $dql = <<<SQL
SELECT user, COUNT(comp) AS components
FROM \Duna\Security\Entity\User user
LEFT JOIN \Duna\Plugin\SecurityComponent\Entity\Permission perm WITH user = perm.user
LEFT JOIN \Duna\Plugin\SecurityComponent\Entity\Component comp WITH perm.component = comp
GROUP BY user
ORDER BY user.id, user.surname, user.name
SQL;
        $result = [];
        foreach ($this->em->createQuery($dql)->getResult() as $row) {
            $result[$row[0]->id] = $row;
        }
        return $result;
    }

    public function getUsersByComponent(array $filters)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select(['user', 'perm'])
            ->from(Permission::class, 'perm')
            ->join('perm.component', 'comp')
            ->innerJoin('perm.user', 'user');

        foreach ($filters['where'] as $filter) {
            $qb->add('where', $filter);
        }
        foreach ($filters['params'] as $key => $value) {
            $qb->setParameter($key, $value);
        }
        return $qb->getQuery()->getResult();
    }

    public function insert($create, $read, $update, $delete, $component, $role = null, $user = null, $throwException = false)
    {
        $entity = new Permission();
        $entity->setCreate($create)
            ->setRead($read)
            ->setUpdate($update)
            ->setDelete($delete);

        try {
            if ($component instanceof Component) {
                $entity->component = $component;
            } elseif (is_numeric($component)) {
                $facade = new ComponentFacade($this->em);
                $entity->component = $facade->getById($component, true);
            } else {
                $facade = new ComponentFacade($this->em);
                $entity->component = $facade->getOneByResource($component, true);
            }

            if ($role === null) {
            } elseif ($role instanceof Role) {
                $entity->role = $role;
            } elseif (is_numeric($role)) {
                $facade = new RoleFacade($this->em);
                $entity->role = $facade->getById($role, true);
            } else {
                $facade = new RoleFacade($this->em);
                $entity->role = $facade->getOneByName($role, true);
            }

            if ($user === null) {
            } elseif ($user instanceof User) {
                $entity->user = $user;
            } elseif (is_numeric($user)) {
                $facade = new UserFacade($this->em);
                $entity->user = $facade->getById($user, true);
            } else {
                $facade = new UserFacade($this->em);
                $entity->user = $facade->getOneByEmail($user, true);
            }
            $this->em->persist($entity);
            $this->em->flush($entity);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }
}