<?php

namespace Duna\Plugin\SecurityComponent\Components;

use Nette\Application\UI\Control;

interface IPermissionUserFormFactory
{
    /**
     * @param \Nette\Application\UI\Control $parent
     * @param $name
     * @param $entity
     * @param \Duna\Plugin\SecurityComponent\Entity\Permission[] $permissions
     * @return PermissionsUserForm\Component
     */
    function create(Control $parent, $name, array $permissions);
}