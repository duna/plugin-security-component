<?php

namespace Duna\Plugin\SecurityComponent\DI;

use App;
use Duna\Console\IEntityConsoleProvider;
use Duna\DI\Extensions\CompilerExtension;
use Duna\Plugin\Manager\Entity\Plugin;
use Duna\Plugin\Manager\IPlugin;
use Duna\Plugin\SecurityComponent\Cli\InstallCommand;
use Duna\Plugin\SecurityComponent\Cli\UninstallCommand;
use Kdyby\Doctrine\DI\IEntityProvider;

class Extension extends CompilerExtension implements IPlugin, IEntityProvider, IEntityConsoleProvider
{
    private $commands = [
        InstallCommand::class,
        UninstallCommand::class,
    ];

    public function beforeCompile()
    {
        $this->addPresenterMapping(['PluginSecurityComponent' => "Duna\\Plugin\\SecurityComponent\\*Module\\Presenters\\*Presenter"]);
    }

    function getEntityMappings()
    {
        return self::entityMappings();
    }

    public function loadConfiguration()
    {
        $this->parseConfig(__DIR__ . '/config.neon');
        $this->addCommands($this->commands);
    }

    /**
     * {@inheritDoc}
     */
    public static function getPluginInfo()
    {
        $entity = new Plugin();
        $entity->name = 'ComponentPermissions';
        $entity->description = 'Plugin for permission component';

        return [
            'plugin' => $entity,
        ];
    }

    public static function entityMappings()
    {
        return ["Duna\\Plugin\\SecurityComponent\\Entity" => __DIR__ . '/../Entity'];
    }
}
