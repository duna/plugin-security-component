<?php

namespace Duna\Plugin\SecurityComponent\Components\PermissionsRoleGrid;

use Duna\Plugin\SecurityComponent\Authorizator;
use Duna\Plugin\SecurityComponent\Entity\Permission;
use Duna\Plugin\SecurityComponent\Entity\PermissionRole;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class Component extends Control
{
    /** @var \Kdyby\Doctrine\EntityManager */
    private $em;
    /** @var Cache */
    private $cache;
    private $cacheAuthorizator;
    private $permissions = [];
    /** Array of where criterias */
    private $filter = [];

    public function __construct(Control $parent, $name, array $filter, EntityManager $em, IStorage $storage)
    {
        parent::__construct($parent, $name);
        $this->em = $em;
        $this->filter = $filter;
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
        //$this->cacheAuthorizator = new Cache($storage, Autorizator::CACHE_NAMESPACE);

        $qb = $this->em->getRepository(Permission::class)->createQueryBuilder('perm', 'perm.id');
        $qb->join('perm.component', 'comp')->addSelect('comp')
            ->join('perm.role', 'role')->addSelect('role')
            ->orderBy('comp.name, role.id');
        foreach ($this->filter['where'] as $filter) {
            $qb->add('where', $filter);
        }
        foreach ($this->filter['params'] as $key => $value) {
            $qb->setParameter($key, $value);
        }
        $this->permissions = $qb->getQuery()
            ->getResult();

    }

    public function render()
    {
        $template = $this->template;
        $template->permissions = $this->permissions;
        $template->render(__DIR__ . '/default.latte');
    }

    public function formSuccess(Form $form, $values)
    {
        $this->cache->clean([
            Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/permissions'],
        ]);
        if (array_key_exists($values->id, $this->permissions)) {
            $entity = $this->permissions[$values->id];
            $entity->setCreate($values->create);
            $entity->setRead($values->read);
            $entity->setUpdate($values->update);
            $entity->setDelete($values->delete);
            $this->em->persist($entity);
            $this->em->flush($entity);
        }
    }

    protected function createComponentAclForm()
    {
        $that = $this;
        return new Multiplier(function ($id) use ($that) {
            $form = new Form();
            $form->addCheckbox($e = 'create', null)->setDefaultValue($that->permissions[$id]->$e);
            $form->addCheckbox($e = 'read', null)->setDefaultValue($that->permissions[$id]->$e);
            $form->addCheckbox($e = 'update', null)->setDefaultValue($that->permissions[$id]->$e);
            $form->addCheckbox($e = 'delete', null)->setDefaultValue($that->permissions[$id]->$e);
            $form->addHidden('id', $id);
            $form->addSubmit('save', 'Uložit');
            $form->onSuccess[] = [$that, 'formSuccess'];
            return $form;
        });
    }

}