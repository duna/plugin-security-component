<?php

namespace Duna\Plugin\SecurityComponent\Components;

use Nette\Application\UI\Control;

interface IPermissionsRoleGridFactory
{
    /** @return PermissionsRoleGrid\Component */
    function create(Control $parent, $name, array $filter);
}