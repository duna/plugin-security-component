<?php

namespace Duna\Plugin\SecurityComponent\Components\PermissionsUserForm;

use Doctrine\ORM\EntityManagerInterface;
use Duna\Forms\ControlForm;
use Duna\Plugin\SecurityComponent\Authorizator;
use Duna\Plugin\SecurityComponent\Entity\PermissionRole;
use Duna\Plugin\SecurityComponent\Facade\PermissionFacade;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class Component extends ControlForm
{
    /** @var Cache */
    private $cache;
    private $permissions = [];
    private $facade;
    private $id = '';
    private $user;

    public function __construct(Control $parent, $name, array $permissions, IStorage $storage, EntityManagerInterface $em)
    {
        parent::__construct($parent, $name, $em, null);
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
        $this->facade = new PermissionFacade($em);
        foreach ($permissions as $permission) {
            $this->permissions[$permission->component->id] = $permission;
            $this->id = '-' . $permission->user->id;
            $this->user = $permission->user;
        }

    }

    public function formError(\Duna\Forms\Form $form)
    {
        // TODO: Implement formError() method.
    }

    public function handlePermissions()
    {
        $this->cache->clean([
            Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/permissions']
        ]);
        $values = (object) $this->presenter->getRequest()->getPost();

        $this->em->beginTransaction();
        foreach ($values->perm_id as $id => $permissionId) {
            $entity = $this->permissions[$id];
            $entity->setCreate(isset($values->create[$id]));
            $entity->setRead(isset($values->read[$id]));
            $entity->setUpdate(isset($values->update[$id]));
            $entity->setDelete(isset($values->delete[$id]));
            $this->em->persist($entity);
            $this->em->flush($entity);
        }
        $this->em->commit();

        foreach ($this->onCreate as $callback) {
            call_user_func($callback);
        }
    }

    public function render()
    {
        $template = $this->template;
        $template->display = true;
        $template->id = $this->id;
        $template->user = $this->user;
        $template->permissions = $this->permissions;
        $template->render(__DIR__ . '/default.latte');
    }

    public function setDefaults(\Duna\Forms\Form $form)
    {
        // TODO: Implement setDefaults() method.
    }

}