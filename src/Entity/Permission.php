<?php

namespace Duna\Plugin\SecurityComponent\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_sec_com_permission")
 * @ORM\Entity
 */
class Permission
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var boolean
     *
     * @ORM\Column(name="c", type="boolean", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $create = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="r", type="boolean", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $read = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="u", type="boolean", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $update = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="d", type="boolean", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $delete = true;
    /**
     * @var \Duna\Plugin\SecurityComponent\Entity\Component
     *
     * @ORM\ManyToOne(targetEntity="Component", cascade={"persist"})
     * @ORM\JoinColumn(name="component_id", referencedColumnName="sec_com_component_id", onDelete="CASCADE")
     */
    protected $component;
    /**
     * @var \Duna\Security\Entity\Role
     *
     * @ORM\ManyToOne(targetEntity="\Duna\Security\Entity\Role", cascade={"persist"})
     * @ORM\JoinColumn(name="role_id", referencedColumnName="role_id", nullable=true, onDelete="CASCADE")
     */
    protected $role;
    /**
     * @var \Duna\Security\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Duna\Security\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id", nullable=true, onDelete="CASCADE")
     */
    protected $user;
    /**
     * @var integer
     *
     * @ORM\Column(name="sec_com_permission_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setCreate($create = true)
    {
        $this->create = (boolean) $create;
        return $this;
    }

    public function setDelete($delete = true)
    {
        $this->delete = (boolean) $delete;
        return $this;
    }

    public function setRead($read = true)
    {
        $this->read = (boolean) $read;
        return $this;
    }

    public function setUpdate($update = true)
    {
        $this->update = (boolean) $update;
        return $this;
    }
}